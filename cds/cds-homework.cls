% cdshomework.cls - style file for CDS homework sets
% RMM, 2 April 1995
%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cds-homework}[2006/01/01 v1.1]

%%
%% Initialization
%%

%%
%% Option declarations and execution
%%
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

%\ExecuteOptions{}
\ProcessOptions

%%
%% Package Loading
%%
\LoadClass{article}

%%
%% Main code
%%

%
% Reset paragraph spacing to something that looks better for problem
% sets (no indentation, some vertical space)
%
\parskip .7em
\parindent 0pt
\topsep .4em
\partopsep 0pt
\itemsep .4em 

%
% Define standard elements of the title
%
\def\instructor#1{\def\instructorname{#1}} \def\instructorname{}
\def\course#1{\def\coursename{#1}} \def\coursename{}
\def\semester#1{\def\semestername{#1}} \def\semestername{}
\def\title#1{\def\titlename{#1}} \def\titlename{}
\def\issued#1{\def\issuedate{Issued: & #1}} \def\issuedate{}
\def\due#1{\def\duedate{Due: & #1}} \def\duedate{}
\def\date#1{\def\issuedate{& #1}}

\def\department#1{\def\departmentname{#1}}
\def\departmentname{Control and Dynamical Systems}
%
% Problem set header
%
\def\maketitle{
  \global\@topnum\z@        % Prevents figures from going at top of page.
  \begin{center}
    CALIFORNIA INSTITUTE OF TECHNOLOGY \\
    \departmentname

    {\large\bf\coursename} \\[1ex]
    \makebox[0pt][l]{\hspace*{-\tabcolsep}\begin{tabular}[t]{l}
      \instructorname \\ \semestername
    \end{tabular}}
    \hfill{\large\bf\titlename}\hfill
    \makebox[0pt][r]{\begin{tabular}[t]{lr}
      \issuedate \\
      \duedate
    \end{tabular}\hspace*{-\tabcolsep}}
  \end{center}
  \thispagestyle{empty}
}

%
% Exercises environment
%
% Reset the spacing for the enumerate environment and add extra space 
% between exercises.
%
\itemsep 0em 
\topsep 0em

\def\exercises{\enumerate}
\def\endexercises{\endenumerate}
\def\newexercise{\par\item}
\def\markexercise[#1]{
  {\refstepcounter{\@enumctr}}
  \item[{#1 \csname label\@enumctr\endcsname}]
}

% Define a command to print point values in the right margin
\def\points#1{\hspace*{0pt}\marginpar{\hfil[#1]\mbox{\quad\quad\quad\quad}}\relax}

%
% Itemized list environment
%
% This is just like itemize, except that the spacing is much tighter
%
\newenvironment{itemlist}%
  {\itemsep 0pt \parsep 0pt \parskip 0pt \topsep 0pt \begin{itemize}}%
  {\end{itemize}}
