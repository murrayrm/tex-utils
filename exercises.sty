% exercises.sty - macros for defining exercises
% RMM, 22 Nov 07; revised 27 Dec 2017
%
% This package contains a set of macros for formatting exercises (and
% their solutions).  The package supports a number of different styles
% for exercises and allows "citations" so that you can keep track of
% references to a given exercise.
%
% The following major environments are defined in this package:
%   * 'exercise'	individual numbered exercise, with tags
%   * 'problem'		problem statement (can omit in solutions)
%   * 'exerpart'	exercise part (with optional tag)
%   * 'exernote'	exercise note (usually instructor note)
%   * 'solution'	solution (omit in problem set)
%
% Exercises can be placed in individual files and included using these commands
%   * \includeexercise	exercise from file, as exercise env (w/ tags)
%   * \inputexercise	exercise from file, w/out exercise env (w/ tags)
% 
% Referencing commands
%   * \exerlabel	exercise label
%   * \exerref		generate reference and citation as marginal note
%   * \exercite		generate citation as marginal note
% 
% Additional commands and environments
%   * \exertitle	exercise title
%   * \exerdbend	mark exercise with dangerous bend sign
%   * 'exercode'	verbatim environment for including code
%   * \exerindex        indexing terms for exercises
%
% Package options
%   * chapter		number exercises using chapter number
%   * solutions		include solutions
%   * noproblems	don't include problem statements
%   * oneperpage	start problems on a new page (useful for solutions)
%   * index		enable indexing commands (exerindex)
%   * notes		include exercise notes (for instructors)
%   * margin		include marginal comments (exerref, exercite, exerpart)
%   * legacy		enable legacy commands
%
% Variables and parameters
%   * exernotecolor	color to use for instructor notes (default = green)
%   * exersolncolor	color to use for solutions (default = blue)

%
% Package Initialization
%
\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{exercises}[2017/12/28 v2.0 exercise macros for LaTeX]

\RequirePackage{amsthm}
\RequirePackage{comment}
\RequirePackage{verbatim}
\RequirePackage{tagging}
\RequirePackage{color}
\RequirePackage{enumitem}
\RequirePackage{mparhack}

%
% Package variables (and initialization)
%

% Define a new colors that we use throughout
\definecolor{exernotecolor}{rgb}{0,0.5,0}	% instructor notes in dark green
\definecolor{exersolncolor}{rgb}{0,0,1}		% solutions in blue

% Names for the various environments
\def\exercisename{Exercise}
\def\solutionname{Solution}
\def\exerpartname{Part}
\def\exernotename{Instructor note}

% Font to use for margin notes
\def\exermarginfont{\raggedright\footnotesize\color{blue}}

%
% Package options
% 

% Option to turn on marginal notes
\newif\ifexermargin\exermarginfalse
\DeclareOption{margin}{\exermargintrue}
\DeclareOption{nomarginpar}{\exermarginfalse} % backward compatibility
\def\exervspace{0pt}			      % parameter to set vertical space

% Option to print with one exercise per page
\newif\ifoneperpage\oneperpagefalse
\DeclareOption{oneperpage}{\oneperpagetrue}

% Option to include header information before each exercise
\newif\ifXRheader\XRheaderfalse
\DeclareOption{header}{\XRheadertrue}

% Option to generate exercise indexing 
\newif\ifexerindex\exerindexfalse
\DeclareOption{index}{
  \exerindextrue
  \def\exerindextag{default}
}

% Option for including solutions
\newif\ifsolutions\solutionsfalse	% See if solutions are being printed
\DeclareOption{solutions}{\XRenablesolutions}

\newif\ifnoproblems\noproblemsfalse	% See if problems are being omitted
\DeclareOption{noproblems}{
  \excludecomment{problem}
  \noproblemstrue
}

% Option to include instructor and grading notes
\newif\ifexernote\exernotefalse		% Exercise notes are off
\DeclareOption{notes}{
  \exernotetrue
  \renewenvironment{exernote}{\begin{exernotex}}{\end{exernotex}}
}

% Options for setting numbering exercises as part of a document
\DeclareOption{chapter}{
  \theoremstyle{exercise}
  \newtheorem{exercisex}{Exercise}[chapter]
}
\DeclareOption{section}{
  \theoremstyle{exercise}
  \newtheorem{exercisex}{Exercise}[section]
}

\DeclareOption{legacy}{
  %
  % Renamed commands
  %
  % \def\exercisename{Problem}		% Defined above (as Exercise)
  \let\points\exerpoints
  \newif\ifallparts\allpartsfalse

  %
  % Renamed environments
  %
  \newenvironment{XRMATLAB}{\exercode}{\endexercode}
  \newenvironment{XRcode}{\exercode}{\endexercode}
  \newenvironment{exerfig}{\center}{\endcenter}
  
  %
  % For instructornote and gradingnote environments, need to handle
  % comment environment
  %
  \ifexernote
    \let\instructornote\exernote \let\endinstructornote\endexernote
    \let\gradingnote\exernote \let\endgradingnote\endexernote
  \else
    \excludecomment{instructornote}
    \excludecomment{gradingnote}
  \fi

  %
  % XRpart environment - used list structure => recreate approximation
  %
  \newcommand\XRdeclarepart[1]{}
  \newcommand\DeclarePart[1]{}
  \newenvironment{XRpart}[1]
    {%
      \ifexermargin\exermarginpar{\exerpartname: #1}\fi%
      \ifallparts\relax\else\taggedblock{#1}\fi%
    }%
    {\ifallparts\else\endtaggedblock\fi\leavevmode}%
}
\DeclareOption{parts}{}				% define as no-op
\DeclareOption{allparts}{\allpartstrue}	% requires legacy option

% 
% Exercise environment
%

\newtheoremstyle{exercise}
  {\topsep}%	Space to leave above the theorem
  {\topsep}%	Space to leave below the theorem
  {}%		Body font
  {}%		Indent amount (empty = no indent, \parindent = para indent)
  {\bfseries}%	Thm head font
  { }%		Punctuation after thm head
  {0ex}%	Space after thm head (\newline = linebreak)
  {\ifXRheader\thmname{#1}\fi\thmnumber{#2} \thmnote{\normalfont(#3)}}

% User callable exercise environment (process options)
\newenvironment{exercise}
  {\ifoneperpage\clearpage\fi\setcounter{enumii}{0}%
    \advance\@enumdepth\@ne%
    \let\oldmargin\leftmargin%
    \let\oldwidth\labelwidth%
    \setlist[enumerate,2]{leftmargin=0pt,align=left,labelwidth=*}%
    \begin{exercisex}}
  {\end{exercisex}%
   \setlist[enumerate,2]{leftmargin=\oldmargin,align=right,labelwidth=\oldwidth}}

%
% Exercise parts
%
%

\newtheoremstyle{exerpart}
  {\itemsep}%	Space to leave above the theorem
  {0pt}%	Space to leave below the theorem
  {}%		Body font
  {}%		Indent amount (empty = no indent, \parindent = para indent)
  {}%		Thm head font
  { }%		Punctuation after thm head
  {0ex}%	Space after thm head (\newline = linebreak)
  {\def\thenumii{#2}\labelenumii}%	Thm head spec

% User callable exercise part environment (process options)
\newenvironment{exerpart}[1][N/A]
  {%
    \ifexermargin\exermarginpar{\exerpartname: #1}\fi%
    \advance\@enumdepth\@ne\begin{exerpartx}%
  }
  {\end{exerpartx}}

\theoremstyle{exerpart}
\newtheorem{exerpartx}[enumii]{\exerpartname}

%
% Additional environments
%

% Problem statement (allows exclusion for solution-only version}
\newenvironment{problem}
  {\endlinechar=`\^^M}		% Blank lines generate paragraphs again
  {}

% Environment for putting code into exercises
\newenvironment{exercode}{\small\verbatim}{\endverbatim\relax}
\newcommand\includeexercode[1]{%
  \bgroup
  \small\verbatiminput{#1}%
  \egroup
}

%
% Solutions
% 

\newtheoremstyle{solution}
  {\itemsep}%		Space to leave above the theorem
  {\itemsep}%		Space to leave below the theorem
  {\color{exersolncolor}}%	Body font
  {}%		Indent amount (empty = no indent, \parindent = para indent)
  {\em}%	Thm head font
  {. }%		Punctuation after thm head
  {0ex}%	Space after thm head (\newline = linebreak)
  {\thmname{#1}}%	Thm head spec

\excludecomment{solution}
\def\XRenablesolutions{
  \theoremstyle{solution}
  \newtheorem*{solutionx}{\solutionname}
  \renewenvironment{solution}
    {\begin{solutionx}%
     \endlinechar=`\^^M}	% Blank lines generate paragraphs again
    {\end{solutionx}}
  \solutionstrue		% In case users want to know this was set
}

%
% Instructor notes
%

\newtheoremstyle{exernote}
  {\itemsep}%		Space to leave above the theorem
  {}%			Space to leave below the theorem
  {\color{exernotecolor}}%	Body font
  {}%		Indent amount (empty = no indent, \parindent = para indent)
  {\em}%	Thm head font
  {:\endlinechar=`\^^M }%	Punctuation after thm head + reset blank lines
  {0ex}%	Space after thm head (\newline = linebreak)
  {\thmname{#1}}%	Thm head spec

\theoremstyle{exernote}
\newtheorem*{exernotex}{\exernotename}
\excludecomment{exernote}		% leave notes off by default

%
% Including exercises from files
%
% Note: The \inputexercise command resets \endlinechar to be catcode 9
% (noop), which means that blank lines in the file do not generate
% paragraph boundaries.  This is useful to allow some spacing between
% the top of the file and the start of the problem without generating
% a line break.  The newline function is turned back on within the
% individual environments that are part of an exercise.
% 

% Define a command for including an exercise
% For now, this is very simple
\newcommand\includeexercise[2][*]{
  \begin{exercise}
  \inputexercise[#1]{#2}
  \end{exercise}
}
\newcommand\inputexercise[2][*]{%
  \bgroup%
  \filename@parse{#2}%
  \edef\exerdir{\filename@area}%  Save exercise directory name
  \edef\exerpath{#2}%		  Save the full path
  \graphicspath{{\exerdir}}%	  Tell \includegraphics where to look
  \usetag{#1}%
  \endlinechar=9%		  Skip blank lines (reset by problem/soln)
  \input{#2}%
  \droptag{#1}%
  \egroup%
}

%
% Macros for referencing and indexing exercises
%

% Macro to create a margin label
\newcommand\exermarginpar[1]{%
  \marginpar{\exermarginfont #1}
}
% Create a separate index for exercises
\newcommand\exercite[1]{%
  \ifexermargin\exermarginpar{\exercisename~\ref{exer:#1}}\fi%
}

% Create exercise labels (standard label + optional marginal note)
\newcommand\exerlabel[2][]{%
  \label{exer:#2}%
  \ifexermargin%
    \marginnote{\exermarginfont%
    \ifx#1\relax\else[#1] #2 \fi%
    \expandafter\csname exind:#2\endcsname}%
  \fi%
}

% Reference an exercise (standard ref + optional marginal note)
\newcommand\exerref[1]{%
  \ref{exer:#1}%
  \ifexermargin\exermarginpar{Exercise~\ref{exer:#1}}\fi%
}

% Indexing commands (add commands to default index)
\newcommand\exerindex[1]{\ifexerindex\index[\exerindextag]{#1}\fi}

%
% Macros for indicating number of points for a problem (or part)
%

% Point problem points in the margin
\def\exerpoints#1{\marginnote{[#1]}}

%
% Miscellaneous additional macros
%

% Macro to create a dangerous bend sign
\font\manual=manfnt
\newcommand\exerdbend{%
  \ \hspace*{-1ex}\marginpar[\hfill{\manual\char127}]{{\manual\char127}}
}

\ProcessOptions\relax
\AtBeginDocument{
  % If exercisex is not yet defined, make sure to define it here
  \ifx\exercisex\undefined
    \theoremstyle{exercise}
    \newtheorem{exercisex}[enumi]{\exercisename}
  \fi
}
